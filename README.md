# OceanPayment 钱海 - CreditCard

2017.11.30 更新

测试账户信息：
Terminal：17056003

secureCode：4228P8V6ZlT622H466h0znr8bd8622088B8864468nfhX44x0d2b26nDpXv80hzn
测试环境：https://secure.oceanpayment.com/gateway/service/test
生产环境：https://secure.oceanpayment.com/gateway/service/pay

---

Account：  170560
Terminal： 17056001
SecureCode： 2l4pv28xT0xBX66pd2j60p2r8lB4VFvjv0b4jz6t4z04b262jb0TBZ642RdlVb68
Transport URL(生产环境)：https://secure.oceanpayment.com/gateway/service/sendTrade
Transport URL(测试环境)：https://secure.oceanpayment.com/gateway/service/testSendTrade



测试信用卡号填：4111111111111111
日期: 12/2020
安全码：123
此卡号仅用于测试环境下的提交

---

![README/opcreditcard.png]